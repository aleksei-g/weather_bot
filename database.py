from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from os import getenv

DB_URI = getenv('DATABASE_URL')

if not DB_URI:
    raise Exception('DB_URI should be specified')


engine = create_engine(DB_URI, echo=False)
session = scoped_session(sessionmaker(autocommit=False,
                                      autoflush=False,
                                      bind=engine))
Base = declarative_base()
Base.query = session.query_property()
