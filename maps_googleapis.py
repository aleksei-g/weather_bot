import requests
import logging
from datetime import datetime
from os import getenv

default_language = getenv('DEFAULT_LANGUAGE', 'EN')
GOOGLE_API_KEY = getenv('GOOGLE_API_KEY')
URL_API_GEOCODE = 'https://maps.googleapis.com/maps/api/geocode/json'
URL_API_TIMEZONE = 'https://maps.googleapis.com/maps/api/timezone/json'
logger = logging.getLogger('root.googleapis')
if not GOOGLE_API_KEY:
    text_log = 'GOOGLE_API_KEY should be specified'
    logger.critical(text_log)
    raise Exception('GOOGLE_API_KEY should be specified')


def get_response_json(url, params=None):
    params['key'] = GOOGLE_API_KEY
    try:
        response = requests.get(url,
                                params=params,
                                timeout=(10, 10))
        logger.debug(response.url)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.ReadTimeout as err:
        error_text = '{}: {}'.format('Oops. Read timeout occured!', err)
        logger.error(error_text)
    except requests.exceptions.ConnectTimeout as err:
        error_text = '{}: {}'.format('Oops. Connection timeout occured!', err)
        logger.error(error_text)
    except requests.exceptions.ConnectionError as err:
        error_text = '{}: {}'.format('Seems like dns lookup failed!', err)
        logger.error(error_text)
    except requests.exceptions.HTTPError as err:
        error_text = '{}\n{} {}'.format('Oops. HTTP Error occured.',
                                        'Response is:',
                                        err.response.content)
        logger.error(error_text)


def get_location_info(lat, lon, lang=default_language):
    params_search = {'latlng': '{},{}'.format(lat, lon),
                     'sensor': True,
                     'language': lang,
                     }
    location_info = get_response_json(URL_API_GEOCODE, params_search)
    if location_info and location_info['status'] == 'OK':
        return location_info


def get_formatted_address_from_googleapis(location_info):
    types_address = {'locality', 'political'}
    try:
        formatted_address = \
            next((location['formatted_address']
                  for location in location_info['results']
                  if set(location['types']).intersection(types_address) ==
                  types_address), None)
        return formatted_address
    except:
        logger.error('Error to get param "formatted_address" from JSON '
                     'location info!')


def get_offset_from_utc_from_googleapis(lat, lon, lang=default_language):
    params_search = {'location': '{},{}'.format(lat, lon),
                     'timestamp': datetime.utcnow().timestamp(),
                     'language': lang,
                     }
    time_zone_info = get_response_json(URL_API_TIMEZONE, params_search)
    if time_zone_info and time_zone_info['status'] == 'OK':
        return time_zone_info['dstOffset'], time_zone_info['rawOffset']
    else:
        return None, None
