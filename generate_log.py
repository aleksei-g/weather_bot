from datetime import datetime

def get_start_bot_log(bot):
    return '******************************\n' \
        '{}: START BOT: {}\n'.format(str(datetime.now()), bot.get_me())


def get_message_log(message, type=None):
    text = []
    text.append(str(datetime.now()))
    text.append('type: {}'.format(type))
    if type == 'CALL':
        text.append('call.id: {}'.format(message.id))
        text.append('message_id: {}'.format(message.message.message_id))
        text.append('chat.id: {}'.format((message.message.chat.id)))
    else:
        text.append('message_id: {}'.format(message.message_id))
        text.append('chat.id: {}'.format((message.chat.id)))
    text.append('FROM user.id: {}'.format(message.from_user.id))
    text.append('first_name: {}'.format(message.from_user.first_name))
    text.append('last_name: {}'.format(message.from_user.last_name))
    text.append('username: {}'.format(message.from_user.username))
    if type == 'CALL':
        text.append('call.data: {} '.format(message.data))
        text.append('message.text: {}'.format(message.message.text))
    else:
        if message.location:
            text.append('message.location: {}'.format(message.location))
        else:
            text.append('message.text: {}'.format(message.text))
    return ', '.join(text)
