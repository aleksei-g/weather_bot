import logging
from pyowm import OWM
from pyowm.webapi25.weather import Weather
from datetime import datetime
from time import time
from os import getenv


OPENWEATHERMAP_KEY = getenv('OPENWEATHERMAP_KEY')
DEFAULT_LANGUAGE = getenv('DEFAULT_LANGUAGE', 'EN')

logger = logging.getLogger('root.weather')
if not OPENWEATHERMAP_KEY:
    text_log = 'OPENWEATHERMAP_KEY should be specified'
    logger.critical(text_log)
    raise Exception('OPENWEATHERMAP_KEY should be specified')
hPa_to_mmHg = 0.75006375541921
yesterday_today_tomorrow = {-1: 'yesterday', 0: 'today', 1: 'tomorrow'}
temp_in_moment = {'temp_min', 'temp_max', 'temp'}
temp_order = {'temp': 0, 'temp_min': 1, 'temp_max': 2,
              'morn': 5, 'day': 6, 'eve': 7, 'night': 8}
# temp_order = {'temp': 0, 'temp_min': 1, 'temp_max': 2, 'min': 3, 'max': 4,
#               'morn': 5, 'day': 6, 'eve': 7, 'night': 8}


def init_owm(lang=DEFAULT_LANGUAGE):
    return OWM(OPENWEATHERMAP_KEY, language=lang)


def get_reference_time_format(self, offset_from_utc=0):
    """
    если запрашивается дата/время для экзепляра погоды из ежедневного прогноза
    нет смысла выводить время. отличить же дневной экземпляр погоды от остальных
    можно лишь по присутсвтию в параметре температуры таких ключей как:
    'night', 'day', 'morn', 'eve'
    """
    unknown_timezone = ''
    if offset_from_utc is None:
        offset_from_utc = 0
        unknown_timezone = ' (UTC)'
    reference_time_with_offset = self._reference_time + offset_from_utc
    day_delta = (datetime.utcfromtimestamp(reference_time_with_offset).date() -
                 datetime.utcfromtimestamp(time() + offset_from_utc)
                 .date()).days

    detail = ' ({})'.format(yesterday_today_tomorrow.get(day_delta)) \
        if yesterday_today_tomorrow.get(day_delta) else ''
    fmt_str = '%a, %d %b{} %H:%M{}'
    if set(self._temperature.keys()).intersection({'night', 'day', 'morn',
                                                   'eve'}):
        fmt_str = '%a, %d %b{}{}'
    return datetime.utcfromtimestamp(reference_time_with_offset)\
        .strftime(fmt_str.format(detail, unknown_timezone))


def get_temperature_format(self):
    temperature = self.get_temperature(unit='celsius')
    temp_key = set(temperature.keys())
    temp_order_key = set(temp_order.keys())
    temperature = {key: temperature[key]
                   for key in temp_key.intersection(temp_order_key)}
    if len(set(temperature.values())) == 1:
        return list(temperature.values())[0]
    elif set(temperature.keys()) == temp_in_moment:
        return '{}... {}'.format(min(temperature.values()),
                                 max(temperature.values()))
    temperature_sorted = sorted(temperature.items(),
                                key=lambda temp: temp_order.get(temp[0], 0))
    temperature_formated = ["{}: {}".format(temp[0], temp[1])
                            for temp in temperature_sorted]
    return ', '.join(temperature_formated)


def get_snow_format(self):
    snow_val = list(self._snow.values())[0] if self._snow else None
    return snow_val


def get_rain_format(self):
    rain_val = list(self._rain.values())[0] if self._rain else None
    return rain_val


def get_pressure_mmhg(self):
    return int(self.get_pressure()['press'] * hPa_to_mmHg)


def get_rhumb(self):
    rhumb = {'N': [337.4, 360.0, 0, 22.4],
             'NE': [22.5, 67.4],
             'E': [67.5, 112.4],
             'SE': [112.5, 157.5],
             'S': [157.5, 202.4],
             'SW': [202.5, 247.4],
             'W': [247.5, 292.4],
             'NW': [292.5, 337.4],
             }
    deg = self.get_wind().get('deg')
    if deg:
        for key, value in rhumb.items():
            if value[0] <= deg <= value[1] or \
                    (len(value) == 4 and (value[2] <= deg <= value[3])):
                return key


def get_currently_observed(lat=None, lon=None, city_id=None, city_name=None,
                           lang=DEFAULT_LANGUAGE):
    owm = init_owm(lang)
    if city_id:
        return owm.weather_at_id(city_id)
    elif lat and lon:
        return owm.weather_at_coords(lat, lon)
    elif city_name:
        return owm.weather_at_place(city_name)


def get_three_hours_forecast_weather(lat=None, lon=None, city_id=None,
                                     city_name=None, period=1,
                                     lang=DEFAULT_LANGUAGE):
    three_hours_in_period = int(period * 24 / 3)
    owm = init_owm(lang)
    if city_id:
        forecast = owm.three_hours_forecast_at_id(city_id).get_forecast()
    elif lat and lon:
        forecast = owm.three_hours_forecast_at_coords(lat, lon).get_forecast()
    elif city_name:
        forecast = owm.three_hours_forecast(city_name).get_forecast()
    forecast._weathers = forecast._weathers[:three_hours_in_period]
    return forecast


def get_daily_forecast_weather(lat=None, lon=None, city_id=None,
                               city_name=None, period=1,
                               lang=DEFAULT_LANGUAGE):
    owm = init_owm(lang)
    if city_id:
        return owm.daily_forecast_at_id(city_id, limit=period).get_forecast()
    elif lat and lon:
        return owm.daily_forecast_at_coords(lat, lon,
                                            limit=period).get_forecast()
    elif city_name:
        return owm.daily_forecast(city_name, limit=period).get_forecast()


def get_weather(lat=None, lon=None, city_id=None, ciny_name=None, period=0,
                forecast_type='current_weather', lang=DEFAULT_LANGUAGE):
    try:
        if forecast_type == 'three_hours_forecast':
            weather = get_three_hours_forecast_weather(lat, lon, city_id,
                                                       ciny_name, period, lang)
        elif forecast_type == 'daily_forecast':
            weather = get_daily_forecast_weather(lat, lon, city_id, ciny_name,
                                                 period, lang)
        else:
            weather = get_currently_observed(lat, lon, city_id, ciny_name, lang)
        return weather
    except Exception as error:
        logger.error(error._message)


def search_cities(city_str):
    owm = init_owm(DEFAULT_LANGUAGE)
    reg = owm.city_id_registry()
    city_dict = [x.strip().upper() for x in city_str.split(',')]
    city = city_dict[0]
    country = city_dict[1] if len(city_dict) == 2 else None
    try:
        return reg.locations_for(city, country)
    except Exception:
        return None


Weather.get_reference_time_format = get_reference_time_format
Weather.get_temperature_format = get_temperature_format
Weather.get_pressure_mmhg = get_pressure_mmhg
Weather.get_rhumb = get_rhumb
Weather.get_rain_format = get_rain_format
Weather.get_snow_format = get_snow_format
