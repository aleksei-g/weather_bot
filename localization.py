import re
localization_dict = {
    # available LANGUAGES
    'available_languages': {'RU': 'русский', 'EN': 'english'},
    # general
    'greetings': {'RU': 'бот приветствует Вас!',
                  'EN': 'bot welcomes you!'},
    'about_bot': {'RU': 'С моей помощью вы получите подробную информацию о '
                        'погоде в любой точке на карте мира. \n'
                        'Введите название города или отправьте мне ваше '
                        'местоположение и вы получите:\n'
                        ' — информацию о текущих погодных условиях,\n'
                        ' — прогноз погоды в формате каждые 3 часа на '
                        'ближайшие 5 дней,\n'
                        ' — ежедневный прогноз погодны на 16 дней.\n'
                        'Для уточнения поискового запроса после '
                        'интересующего вас населенного пункта через запятую '
                        'укажите код страны. Например:\n'
                        '`London, UK`.',
                  'EN': 'I give you detailed weather information for your '
                        'city.\n'
                        'Enter city name or send me your location and you will '
                        'get:\n'
                        ' — current weather data,\n'
                        ' — weather in format every 3 hours for the next 5 '
                        'days,\n'
                        ' — daily forecast weather for 16 days.\n'
                        'To refine your search you can add the code of '
                        'the country to the city. For example:\n'
                        '`London, UK`.'},
    'list_commands': {'RU': 'Вот доступный список команд',
                      'EN': 'Here is a list of available commands'},
    'start_command': {'RU': 'Начать общение с ботом',
                      'EN': 'Start chat with the bot'},
    'stop_command': {'RU': 'Прекратить общение с ботом. '
                           'Сброс пользовательских настроек.',
                     'EN': 'Stop chat with the bot. Reset users settings.'},
    'setlang_command': {'RU': 'Выбрать язык интерфейса',
                        'EN': 'Select language interface'},
    'addcity_command': {'RU': 'Добавить город в избранное',
                        'EN': 'Add city to favorite list'},
    'delcity_command': {'RU': 'Удалить город из избранного',
                        'EN': 'Delete city from favorite list'},
    'keyboard_command': {'RU': 'Показать клавиатуру',
                         'EN': 'Show keyboard'},
    'help_command': {'RU': 'Информация', 'EN': 'Information'},
    'show_keyboard': {'RU': 'Показана пользовательская клавиатура.',
                      'EN': 'Custom keyboard show.'},
    'choice_city_to_del': {'RU': 'Выберите город для удаления',
                           'EN': 'Select a city to remove'},
    'favorites_is_empty': {'RU': 'Список избранного пуст.',
                           'EN': 'List of favorite is empty.'},
    'removed_from_favorites': {'RU': 'удален из избранного.',
                               'EN': 'removed from the list of favorites.'},
    'error_remove_from_favorites': {'RU': 'Ошибка при удалении из избранного.',
                                    'EN': 'Error removing from list of '
                                          'favorites.'},
    'set_default_city_text': {'RU': 'Отправьте ваши координаты или введите '
                                    'название города, чтобы добавить его в '
                                    'избранное.',
                              'EN': 'Send your location or enter the city name '
                                    'to add it to favorites.'},
    'cancel': {'RU': 'Отмена', 'EN': 'Cancel'},
    'push_cancel': {'RU': 'Ваши настройки не были изменены.',
                          'EN': 'Your settings have not been changed.'},
    'run_start': {'RU': 'Для начала работы воспользуйтесь командой /start',
                  'EN': 'To get started use the command /start'},
    'send_location': {'RU': 'Отправить ваше местоположение боту',
                      'EN': 'Send your location'},
    'favorite': {'RU': 'Избранное', 'EN': 'Favorite'},
    'your_favorite_cities': {'RU': 'Ваш список городов в избранном',
                             'EN': 'Your favorite cities'},
    'received_stop_command': {'RU': 'Получена команда /stop. '
                                    'Настройки пользователя сброшены.',
                              'EN': 'Received the /stop command. '
                                    'User settings have been reset.'},
    'select_language': {'RU': 'Выберите язык', 'EN': 'Select language'},
    'selected_language': {'RU': 'Выбранный язык', 'EN': 'Selected language'},
    'save_settings': {'RU': 'Настройки обновлены.',
                      'EN': 'Setting updated'},
    'select_period': {'RU': 'Выберите период', 'EN': 'Select period'},
    'select_forecast_type': {'RU': 'Выберите тип прогноза погоды',
                             'EN': 'Select the type of forecast weather'},
    'select_city': {'RU': 'Выберите город', 'EN': 'Select city'},
    'city_added_to_favorites': {'RU': 'был добавлен в избранное.',
                                'EN': 'was added to the list of favorites.'},
    'your_choice': {'RU': 'Ваш выбор', 'EN': 'Your choice'},
    # param OWM
    'temp': {'RU': 'сред', 'EN': 'mid'},
    'temp_min': {'RU': 'мин', 'EN': 'min'},
    'temp_max': {'RU': 'макс', 'EN': 'max'},
    'morn': {'RU': 'утро', 'EN': 'morning'},
    'night': {'RU': 'ночь', 'EN': 'night'},
    'eve': {'RU': 'вечер', 'EN': 'eve'},
    'day': {'RU': 'день', 'EN': 'day'},
    'max': {'RU': 'макс', 'EN': 'max'},
    'min': {'RU': 'мин', 'EN': 'min'},
    # param rhumb
    'N': {'RU': 'С', 'EN': 'N'},
    'NE': {'RU': 'СВ', 'EN': 'NE'},
    'E': {'RU': 'В', 'EN': 'E'},
    'SE': {'RU': 'ЮВ', 'EN': 'SE'},
    'S': {'RU': 'Ю', 'EN': 'S'},
    'SW': {'RU': 'ЮЗ', 'EN': 'SW'},
    'W': {'RU': 'З', 'EN': 'W'},
    'NW': {'RU': 'СЗ', 'EN': 'NW'},
    # param templates
    'temperature': {'RU': 't, ℃', 'EN': 't, ℃'},
    'overcast': {'RU': 'облачность', 'EN': 'overcast'},
    'precipitation': {'RU': 'осадки', 'EN': 'precipitation'},
    'rain': {'RU': 'дождь', 'EN': 'rain'},
    'snow': {'RU': 'снег', 'EN': 'snow'},
    'humidity': {'RU': 'влажность', 'EN': 'humidity'},
    'pressure': {'RU': 'давление', 'EN': 'pressure'},
    'mmhg': {'RU': 'мм рт.ст.', 'EN': 'mmHg.'},
    'wind': {'RU': 'ветер', 'EN': 'wind'},
    'metersPerSecond': {'RU': 'м/с', 'EN': 'm/s'},
    # forecast_type
    'current_weather': {'RU': 'текущая погода', 'EN': 'current weather'},
    'three_hours_forecast': {'RU': 'каждые 3 часа / 5 дней',
                             'EN': 'every 3 hours / 5 day'},
    'daily_forecast': {'RU': 'ежедневный прогноз / 16 дней',
                       'EN': 'daily forecast / 16 day'},
    # periods
    '1_day': {'RU': '1 день', 'EN': '1 day'},
    '2_day': {'RU': '2 дня', 'EN': '2 day'},
    '3_day': {'RU': '3 дня', 'EN': '3 day'},
    '4_day': {'RU': '4 дня', 'EN': '4 day'},
    '5_day': {'RU': '5 дней', 'EN': '5 day'},
    '6_day': {'RU': '6 дней', 'EN': '6 day'},
    '7_day': {'RU': '7 дней', 'EN': '7 day'},
    '8_day': {'RU': '8 дней', 'EN': '8 day'},
    '9_day': {'RU': '9 дней', 'EN': '9 day'},
    '10_day': {'RU': '10 дней', 'EN': '10 day'},
    '11_day': {'RU': '11 дней', 'EN': '11 day'},
    '12_day': {'RU': '12 дней', 'EN': '12 day'},
    '13_day': {'RU': '13 дней', 'EN': '13 day'},
    '14_day': {'RU': '14 дней', 'EN': '14 day'},
    '15_day': {'RU': '15 дней', 'EN': '15 day'},
    '16_day': {'RU': '16 дней', 'EN': '16 day'},
    # months
    'Jan': {'RU': 'Янв.', 'EN': 'Jan.'},
    'Feb': {'RU': 'Фев.', 'EN': 'Feb.'},
    'Mar': {'RU': 'Мар.', 'EN': 'Mar.'},
    'Apr': {'RU': 'Апр.', 'EN': 'Apr.'},
    'May': {'RU': 'Май.', 'EN': 'May.'},
    'Jun': {'RU': 'Июн.', 'EN': 'Jun.'},
    'Jul': {'RU': 'Июл.', 'EN': 'Jul.'},
    'Aug': {'RU': 'Авг.', 'EN': 'Aug.'},
    'Sep': {'RU': 'Сен.', 'EN': 'Sep.'},
    'Oct': {'RU': 'Окт.', 'EN': 'Oct.'},
    'Nov': {'RU': 'Ноя.', 'EN': 'Nov.'},
    'Dec': {'RU': 'Дек.', 'EN': 'Dec.'},
    # days
    'Mon': {'RU': 'Пн', 'EN': 'Mo'},
    'Tue': {'RU': 'Вт', 'EN': 'Tu'},
    'Wed': {'RU': 'Ср', 'EN': 'We'},
    'Thu': {'RU': 'Чт', 'EN': 'Th'},
    'Fri': {'RU': 'Пт', 'EN': 'Fr'},
    'Sat': {'RU': 'Сб', 'EN': 'Sa'},
    'Sun': {'RU': 'Вс', 'EN': 'Su'},
    # yesterday_today_tomorrow
    'yesterday': {'RU': 'вчера', 'EN': 'yesterday'},
    'today': {'RU': 'сегодня', 'EN': 'today'},
    'tomorrow': {'RU': 'завтра', 'EN': 'tomorrow'},
    # ERROR
    'error_get_weather_by_pyown':
        {'RU': 'Что-то пошло не так. '
               'Попробуйте изменить запрос или повторить его позже.',
         'EN': 'Error retrieving information from server. '
               'Try to change the query, or repeat it later.'},
    }


def get_localized_param(param, lang='EN'):
    return localization_dict.get(param, {lang: param}).get(lang, param)


def translate_text(text_raw, lang='EN'):
    text = re.sub(r'\b\w+',
                  lambda x: get_localized_param(x.group(), lang),
                  text_raw)
    return text


def get_available_languages():
    return localization_dict['available_languages']
