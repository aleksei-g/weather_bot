from telebot import TeleBot
from telebot import apihelper
from telebot.types import ReplyKeyboardMarkup, InlineKeyboardMarkup, \
    ReplyKeyboardRemove, KeyboardButton, InlineKeyboardButton, CallbackQuery, \
    Update
from flask import Flask, request
from get_templates import greetings_tmpl, main_tmpl
from generate_log import get_message_log, get_start_bot_log
from weather_by_pyown import get_weather, search_cities
from localization import translate_text, get_available_languages
from db_function import add_user, deactivate_user, add_params_to_query, \
    user_is_active, get_user_language, set_user_language, get_user_cities, \
    add_user_city_favorite, del_user_city_favorite, get_query, delete_query, \
    get_type_query, get_city_offset_from_utc, add_city_offset_from_utc, \
    get_city_formatted_address, add_city_formatted_address
import re
import logging.config
from localization import localization_dict
from maps_googleapis import get_offset_from_utc_from_googleapis, \
    get_formatted_address_from_googleapis, get_location_info
from os import getenv

BOT_TOKEN = getenv('BOT_TOKEN')
BOT_PROXY = getenv('BOT_PROXY')
URL_WEBHOOK = getenv('URL_WEBHOOK')
PORT = getenv('PORT', 5000)
DEFAULT_LANGUAGE = getenv('DEFAULT_LANGUAGE', 'EN')


logging.config.fileConfig('logging.config')
logger = logging.getLogger('root')
if not BOT_TOKEN:
    text_log = 'BOT_TOKEN should be specified'
    logger.critical(text_log)
    raise Exception('BOT_TOKEN should be specified')


FORECAST_TYPES = {'current_weather': 0, 'three_hours_forecast': 5,
                  'daily_forecast': 16}
PERIODS = [(x, '{}_day'.format(x))
           for x in range(1, max(FORECAST_TYPES.values()) + 1)]
LANGUAGES = get_available_languages()
LANGUAGES_CODE = list(LANGUAGES.keys())
server = Flask(__name__)
if BOT_PROXY:
    apihelper.proxy = {'https': BOT_PROXY}
bot = TeleBot(BOT_TOKEN)
logger.info(get_start_bot_log(bot))


def create_general_keyboard(user, lang=DEFAULT_LANGUAGE):
    button_list = [{'button_text': 'send_location', 'request_location': True}]
    if get_user_cities(user):
        button_list.append({'button_text': 'favorite',
                            'request_location': False})
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    keyboard.add(*[KeyboardButton(text=
                                  translate_text(button_param['button_text'],
                                                 lang),
                                  request_location=
                                  button_param['request_location'])
                   for button_param in button_list])
    return keyboard


def create_send_location_cancel_keyboard(lang):
    buttons = [('send_location', True), ('cancel', False)]
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    keyboard.add(*[KeyboardButton(text=translate_text(name, lang),
                                  request_location=loc)
                   for name, loc in buttons])
    return keyboard


def create_forecast_types_keyboard(lang=DEFAULT_LANGUAGE):
    forecast_types_keyboard = InlineKeyboardMarkup(row_width=1)
    sorted_forecast_types = [y[0] for y in sorted(FORECAST_TYPES.items(),
                                                  key=lambda x: x[1])]
    buttons = [InlineKeyboardButton(text=translate_text(forecast_type, lang),
                                    callback_data=forecast_type)
               for forecast_type in sorted_forecast_types]
    forecast_types_keyboard.add(*buttons)
    return forecast_types_keyboard


def answer_forecast_types_keyboard(message, user):
    forecast_types_keyboard = \
        create_forecast_types_keyboard(get_user_language(user))
    answer = translate_text('select_forecast_type:', get_user_language(user))
    return bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                            reply_markup=forecast_types_keyboard)


def create_periods_keyboard(forecast_type, lang=DEFAULT_LANGUAGE):
    periods_keyboard = InlineKeyboardMarkup(row_width=4)
    buttons = [InlineKeyboardButton(text=translate_text(str(period[1]), lang),
                                    callback_data=str(period[0]))
               for period in PERIODS[:FORECAST_TYPES[forecast_type]]]
    periods_keyboard.add(*buttons)
    return periods_keyboard


def create_language_keyboard():
    languages_keyboard = InlineKeyboardMarkup(row_width=2)
    buttons = [InlineKeyboardButton(text=str(language),
                                    callback_data=str(language_code))
               for language_code, language in LANGUAGES.items()]
    languages_keyboard.add(*buttons)
    return languages_keyboard


def create_cities_keyboard(cities, name_param='weather_city_id'):
    cities_keyboard = InlineKeyboardMarkup(row_width=1)
    buttons = [InlineKeyboardButton(text=str(city_address),
                                    callback_data=
                                    '{}={}'.format(name_param, city_id))
               for city_id, city_address in cities.items()]
    cities_keyboard.add(*buttons)
    return cities_keyboard


def check_user(func):
    def check_user_is_active(message):
        if user_is_active(message.from_user):
            func(message)
        else:
            logger.info(get_message_log(message, 'MESSAGE'))
            answer_text = translate_text('run_start',
                                         get_user_language(message.from_user))
            answer = bot.send_message(message.chat.id, answer_text)
            logger.info(get_message_log(answer, 'ANSWER'))
    return check_user_is_active


def bot_log_info(func):
    def log_info(message):
        type_message = 'MESSAGE'
        if type(message) == CallbackQuery:
            type_message = 'CALL'
        logger.info(get_message_log(message, type_message))
        answer = func(message)
        logger.info(get_message_log(answer, 'ANSWER'))
    return log_info


def set_default_city(user):
    query = get_query(user)
    weather = get_weather(lat=query.city_lat, lon=query.city_lon,
                          city_id=query.city_id, ciny_name=query.city_name,
                          forecast_type=query.forecast_type,
                          period=query.period, lang=query.user.language)
    if weather is None:
        logger.error('Error retrieving information "get_weather" from server.')
        return None
    delete_query(user)
    city_id = weather.get_location().get_ID()
    city_address = get_formatted_address(weather.get_location(),
                                         lang=get_user_language(user))
    add_user_city_favorite(user, city_id, city_address)
    return city_address


def answer_set_default_city(user, chat_id, city_address, lang=DEFAULT_LANGUAGE):
    keyboard = create_general_keyboard(user, lang)
    answer = translate_text('*{}* {}'.format(city_address,
                                             'city_added_to_favorites'),
                            lang)
    if city_address is None:
        answer = translate_text('error_get_weather_by_pyown', lang=lang)
        keyboard = None
    return bot.send_message(chat_id, answer, parse_mode='Markdown',
                            reply_markup=keyboard)


def get_formatted_address(location, lang=DEFAULT_LANGUAGE):
    address = {}
    address_dict = get_city_formatted_address(location.get_ID())
    if address_dict:
        address = address_dict.get(lang)
    if not address:
        address = \
            get_formatted_address_from_googleapis(
                get_location_info(location.get_lat(), location.get_lon(), lang))
        if address:
            add_city_formatted_address(location.get_ID(), address, lang)
    return address if address else '{} ({})'.format(location.get_name(),
                                                    location.get_country())


def get_offset_from_utc(location, lang=DEFAULT_LANGUAGE):
    dst_offset, raw_offset = get_city_offset_from_utc(location.get_ID())
    if dst_offset is None and raw_offset is None:
        dst_offset, raw_offset = \
            get_offset_from_utc_from_googleapis(lat=location.get_lat(),
                                                lon=location.get_lon(),
                                                lang=lang)
        if dst_offset and raw_offset:
            add_city_offset_from_utc(location.get_ID(), dst_offset, raw_offset)
        else:
            return 0
    return dst_offset + raw_offset


def get_weather_by_pyown(user):
    query = get_query(user)
    weather = get_weather(lat=query.city_lat, lon=query.city_lon,
                          city_id=query.city_id, ciny_name=query.city_name,
                          forecast_type=query.forecast_type,
                          period=query.period, lang=query.user.language)
    delete_query(user)
    if weather is None:
        answer_raw = 'error_get_weather_by_pyown'
        logger.error('Error retrieving information from server.')
    else:
        formatted_address = get_formatted_address(weather.get_location(),
                                                  lang=query.user.language)
        offset_from_utc = get_offset_from_utc(weather.get_location(),
                                              lang=query.user.language)
        answer_raw = main_tmpl.render(fc=weather,
                                      formatted_address=formatted_address,
                                      offset_from_utc=offset_from_utc)
    return translate_text(answer_raw, get_user_language(user))


@bot.message_handler(commands=['start', 'help'])
@bot_log_info
def start_command(message):
    add_user(message.from_user)
    keyboard = create_general_keyboard(message.from_user,
                                       get_user_language(message.from_user))
    bot_name = bot.get_me().first_name if bot.get_me().first_name \
        else bot.get_me().username
    answer_raw = greetings_tmpl.render(bot_username=bot_name)
    answer = translate_text(answer_raw, get_user_language(message.from_user))
    return bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                            reply_markup=keyboard)


@bot.message_handler(commands=['keyboard'])
@bot_log_info
def keyboard_command(message):
    keyboard = create_general_keyboard(message.from_user,
                                       get_user_language(message.from_user))
    answer = translate_text('show_keyboard',
                            get_user_language(message.from_user))
    return bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                            reply_markup=keyboard)


@bot.message_handler(commands=['stop'])
@bot_log_info
def stop_command(message):
    lang = get_user_language(message.from_user)
    deactivate_user(message.from_user)
    keyboard_hide = ReplyKeyboardRemove()
    answer = translate_text('received_stop_command', lang)
    return bot.send_message(message.chat.id, answer,
                            reply_markup=keyboard_hide)


@bot.message_handler(commands=['setlang'])
@check_user
@bot_log_info
def set_language_command(message):
    languages_keyboard = create_language_keyboard()
    answer = translate_text('select_language:',
                            get_user_language(message.from_user))
    return bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                            reply_markup=languages_keyboard)


@bot.message_handler(commands=['addcity'])
@check_user
@bot_log_info
def add_city_command(message):
    add_params_to_query(user=message.from_user, type_query='set_city')
    keyboard = create_send_location_cancel_keyboard(
        get_user_language(message.from_user))
    answer = translate_text('set_default_city_text',
                            get_user_language(message.from_user))
    message = bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                               reply_markup=keyboard)
    return message


@bot.message_handler(commands=['delcity'])
@check_user
@bot_log_info
def del_city_command(message):
    cities = get_user_cities(message.from_user)
    if cities:
        answer_raw = 'choice_city_to_del'
    else:
        answer_raw = 'favorites_is_empty'
    answer = translate_text(answer_raw,
                            lang=get_user_language(message.from_user))
    cities_keyboard = create_cities_keyboard(cities, name_param='del_city_id')
    return bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                            reply_markup=cities_keyboard)


@bot.message_handler(content_types=['location'])
@check_user
@bot_log_info
def location(message):
    lang = get_user_language(message.from_user)
    param = {'user': message.from_user, 'city_lat': message.location.latitude,
             'city_lon': message.location.longitude}
    add_params_to_query(**param)
    if get_type_query(message.from_user) == 'set_city':
        city_address = set_default_city(message.from_user)
        return answer_set_default_city(message.from_user, message.chat.id,
                                       city_address, lang=lang)
    return answer_forecast_types_keyboard(message, message.from_user)


@bot.message_handler(content_types=['text'],
                     func=lambda message:
                     message.text in localization_dict['cancel'].values())
@check_user
@bot_log_info
def push_cancel_set_city(message):
    delete_query(message.from_user)
    keyboard = create_general_keyboard(message.from_user,
                                       get_user_language(message.from_user))
    answer = translate_text('push_cancel',
                            get_user_language(message.from_user))
    return bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                            reply_markup=keyboard)


@bot.message_handler(content_types=['text'],
                     func=lambda message:
                     message.text in localization_dict['favorite'].values())
@check_user
@bot_log_info
def push_favorite_button(message):
    cities = get_user_cities(message.from_user)
    if cities:
        answer_raw = 'your_favorite_cities:'
    else:
        answer_raw = 'favorites_is_empty'
    answer = translate_text(answer_raw,
                            lang=get_user_language(message.from_user))
    cities_keyboard = create_cities_keyboard(cities)
    return bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                            reply_markup=cities_keyboard)


@bot.message_handler(content_types=['text'])
@check_user
@bot_log_info
def find_by_city(message):
    lang = get_user_language(message.from_user)
    add_params_to_query(user=message.from_user, city_name=message.text)
    cities_locs = search_cities(message.text)
    if cities_locs and len(cities_locs) > 1:
        cities = {city.get_ID(): get_formatted_address(city, lang)
                  for city in cities_locs}
        cities_keyboard = create_cities_keyboard(cities)
        answer = translate_text('select_city:',
                                get_user_language(message.from_user))
        if cities_keyboard is None:
            answer = translate_text('error_get_weather_by_pyown',
                                    get_user_language(message.from_user))
        return bot.send_message(message.chat.id, answer, parse_mode='Markdown',
                                reply_markup=cities_keyboard)
    elif cities_locs and len(cities_locs) == 1:
        add_params_to_query(user=message.from_user,
                            city_id=cities_locs[0].get_ID())
    else:
        add_params_to_query(user=message.from_user,
                            city_name=message.text)
    if get_type_query(message.from_user) == 'set_city':
        city_address = set_default_city(message.from_user)
        return answer_set_default_city(message.from_user, message.chat.id,
                                       city_address,
                                       lang=
                                       get_user_language(message.from_user))
    return answer_forecast_types_keyboard(message, message.from_user)


@bot.callback_query_handler(func=lambda call: True
                            and call.data in LANGUAGES_CODE)
@check_user
@bot_log_info
def language_callback_inline(call):
    set_user_language(user=call.from_user,
                      lang=call.data)
    keyboard = create_general_keyboard(call.from_user, call.data)
    answer_raw = '{}: {}'.format('selected_language', LANGUAGES[call.data])
    answer = translate_text(answer_raw, lang=call.data)
    answer_message = bot.edit_message_text(chat_id=call.message.chat.id,
                                           message_id=call.message.message_id,
                                           text=answer)
    bot.send_message(chat_id=call.message.chat.id,
                     text=translate_text('save_settings', call.data),
                     reply_markup=keyboard)
    return answer_message


@bot.callback_query_handler(func=lambda call: True
                            and call.data.startswith('del_city_id='))
@check_user
@bot_log_info
def del_city_callback_inline(call):
    city_id = int(re.sub('del_city_id=', '', call.data))
    deleted_city_address = del_user_city_favorite(call.from_user, city_id)
    if deleted_city_address:
        answer_raw = '*{}* {}'.format(deleted_city_address,
                                      'removed_from_favorites')
    else:
        answer_raw = '*{}* {}'.format(deleted_city_address,
                                      'error_remove_from_favorites')
    answer = translate_text(answer_raw, lang=get_user_language(call.from_user))
    answer_message = bot.edit_message_text(chat_id=call.message.chat.id,
                                           message_id=call.message.message_id,
                                           text=answer,
                                           parse_mode='Markdown')
    keyboard = create_general_keyboard(call.from_user,
                                       get_user_language(call.from_user))
    bot.send_message(chat_id=call.message.chat.id,
                     text=translate_text('save_settings',
                                         get_user_language(call.from_user)),
                     reply_markup=keyboard)
    return answer_message


@bot.callback_query_handler(func=lambda call: True
                            and call.data.startswith('weather_city_id='))
@check_user
@bot_log_info
def city_callback_inline(call):
    city_id = int(re.sub('weather_city_id=', '', call.data))
    add_params_to_query(user=call.from_user, city_id=city_id)
    if get_type_query(call.from_user) == 'set_city':
        city_address = set_default_city(call.from_user)
        answer = translate_text('{}: {}'.format('your_choice', city_address),
                                lang=get_user_language(call.from_user))
        bot.edit_message_text(chat_id=call.message.chat.id,
                              message_id=call.message.message_id,
                              text=answer, parse_mode='Markdown')
        return answer_set_default_city(call.from_user, call.message.chat.id,
                                       city_address,
                                       lang=get_user_language(call.from_user))
    forecast_types_keyboard = \
        create_forecast_types_keyboard(get_user_language(call.from_user))
    answer = translate_text('select_forecast_type:',
                            get_user_language(call.from_user))
    return bot.edit_message_text(chat_id=call.message.chat.id,
                                 message_id=call.message.message_id,
                                 text=answer, parse_mode='Markdown',
                                 reply_markup=forecast_types_keyboard)


@bot.callback_query_handler(func=lambda call: True
                            and call.data in FORECAST_TYPES)
@check_user
@bot_log_info
def forecast_type_callback_inline(call):
    add_params_to_query(user=call.from_user,
                        forecast_type=call.data)
    if FORECAST_TYPES[call.data] == 0:
        answer = get_weather_by_pyown(call.from_user)
        return bot.edit_message_text(chat_id=call.message.chat.id,
                                     message_id=call.message.message_id,
                                     text=answer, parse_mode='Markdown')
    periods_keyboard = \
        create_periods_keyboard(forecast_type=call.data,
                                lang=get_user_language(call.from_user))
    answer = translate_text('select_period:',
                            get_user_language(call.from_user))
    return bot.edit_message_text(chat_id=call.message.chat.id,
                                 message_id=call.message.message_id,
                                 text=answer, parse_mode='Markdown',
                                 reply_markup=periods_keyboard)


@bot.callback_query_handler(func=lambda call: True
                            and call.data in [str(period[0])
                                              for period in PERIODS])
@check_user
@bot_log_info
def period_callback_inline(call):
    add_params_to_query(user=call.from_user, period=int(call.data))
    answer = get_weather_by_pyown(call.from_user)
    return bot.edit_message_text(chat_id=call.message.chat.id,
                                 message_id=call.message.message_id,
                                 text=answer, parse_mode='Markdown')


@server.route("/{}".format(BOT_TOKEN), methods=['POST'])
def get_message():
    bot.process_new_updates([Update.de_json(request.stream.read()
                                            .decode("utf-8"))])
    return "!", 200


@server.route("/setwebhook")
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url=URL_WEBHOOK)
    return "!", 200


@server.route("/")
def index():
    return "get_weather_bot", 200


if URL_WEBHOOK:
    server.run(host="0.0.0.0", port=int(PORT))
    server = Flask(__name__)
else:
    bot.polling(none_stop=True)
