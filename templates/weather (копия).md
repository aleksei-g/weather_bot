*{{ weather.get_reference_time_format() }}*
temperature: {{ weather.get_temperature_format() }};
overcast: {{ weather.get_clouds() }}%;
{{ weather.get_detailed_status() }};{% if weather.get_rain_format() %} rain: {{ weather.get_rain_format() }};{% endif %}{% if weather.get_snow_format() %} snow: {{ weather.get_snow_format() }};{% endif %}
humidity: {{ weather.get_humidity() }}%;
pressure: {{ weather.get_pressure_mmhg() }} mmhg;
wind: {% if weather.get_wind().get('deg') %}{{ weather.get_rhumb() }}, {% endif %}{{ weather.get_wind()['speed'] }} metersPerSecond