*{{ weather.get_reference_time_format(offset_from_utc) }}*
temperature: {{ weather.get_temperature_format() }};
overcast: {{ weather.get_clouds() }}%; {{ weather.get_detailed_status() }};
{{ weather.get_pressure_mmhg() }} mmhg; wind: {% if weather.get_wind().get('deg') %}{{ weather.get_rhumb() }}, {% endif %}{{ weather.get_wind()['speed'] }} metersPerSecond