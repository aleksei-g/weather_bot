from sqlalchemy import Column, Integer, Float, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from database import Base
from datetime import datetime

from os import getenv

DEFAULT_LANGUAGE = getenv('DEFAULT_LANGUAGE', 'EN')


class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=False)
    first_name = Column(String(50))
    last_name = Column(String(50))
    username = Column(String(50))
    registration_datetime = Column(DateTime(timezone=False), nullable=False,
                                   default=datetime.utcnow)
    deactivation_datetime = Column(DateTime(timezone=False))
    cities = Column(String())
    language = Column(String(5), default=DEFAULT_LANGUAGE)
    last_use_datetime = Column(DateTime(timezone=False))
    query_count = Column(Integer, default=0)
    query = relationship('Query', uselist=False, back_populates='user')

    def is_active(self):
        return not self.deactivation_datetime


class Query(Base):
    __tablename__ = 'query'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), unique=True)
    user = relationship("Users", back_populates="query")
    create_datetime = Column(DateTime(timezone=False), nullable=False,
                             default=datetime.utcnow)
    type_query = Column(String(30), nullable=False, default='get_weather')
    city_id = Column(Integer)
    city_name = Column(String(100))
    city_lat = Column(Float)
    city_lon = Column(Float)
    forecast_type = Column(String(30))
    period = Column(Integer)


class Cities(Base):
    __tablename__ = 'cities'
    id = Column(Integer, primary_key=True, autoincrement=False)
    formatted_address = Column(String())
    dst_offset_from_utc = Column(Integer)
    raw_offset_from_utc = Column(Integer)
    address_update_after_datetime = Column(DateTime(timezone=False))
    offset_update_after_datetime = Column(DateTime(timezone=False))
