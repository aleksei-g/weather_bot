from jinja2 import Environment, FileSystemLoader


env = Environment(loader=FileSystemLoader('templates'))
greetings_tmpl = env.get_template('greetings.md')
main_tmpl = env.get_template('main.md')
