from database import session, Base, engine
from models import Users, Query, Cities
from datetime import datetime, timedelta
import json
from os import getenv

DEFAULT_LANGUAGE = getenv('DEFAULT_LANGUAGE', 'EN')
OFFSET_UPDATE_INTERVAL = timedelta(days=1)
ADDRESS_UPDATE_INTERVAL = timedelta(days=1) * 30


def init_db():
    Base.metadata.create_all(bind=engine)


def user_is_active(user):
    user_in_db = session.query(Users).get(user.id)
    return user_in_db and user_in_db.is_active()


def get_user_language(user):
    user_in_db = session.query(Users).get(user.id)
    if user_in_db and user_in_db.language:
        return user_in_db.language
    else:
        return DEFAULT_LANGUAGE


def set_user_language(user, lang):
    user_in_db = session.query(Users).get(user.id)
    if user_in_db:
        user_in_db.language = lang
        session.commit()


def get_user_cities(user):
    cities = {}
    user_in_db = session.query(Users).get(user.id)
    if user_in_db:
        try:
            cities = json.loads(user_in_db.cities)
        except:
            pass
    return cities


def add_user_city_favorite(user, city_id, city_address):
    user_in_db = session.query(Users).get(user.id)
    if user_in_db:
        cities = get_user_cities(user)
        cities[city_id] = city_address
        user_in_db.cities = json.dumps(cities)
        session.commit()


def del_user_city_favorite(user, city_id):
    city_id = str(city_id)
    user_in_db = session.query(Users).get(user.id)
    if user_in_db:
        cities = get_user_cities(user)
        if city_id in cities:
            deleted_city_address = cities[city_id]
            del cities[city_id]
            user_in_db.cities = json.dumps(cities)
            session.commit()
            return deleted_city_address


def add_user(new_user):
    user = session.query(Users).get(new_user.id)
    if not user:
        user = Users(**new_user.__dict__)
    user.deactivation_datetime = None
    session.add(user)
    session.commit()
    return user


def deactivate_user(user):
    user_in_db = session.query(Users).get(user.id)
    if user_in_db and user_in_db.deactivation_datetime is None:
        delete_query(user_in_db)
        user_in_db.deactivation_datetime = datetime.utcnow()
        user_in_db.cities = ''
        user_in_db.language = DEFAULT_LANGUAGE
        session.add(user_in_db)
        session.commit()


def get_query(user):
    user_in_db = session.query(Users).get(user.id)
    if user_in_db:
        return user_in_db.query


def get_type_query(user):
    user_in_db = session.query(Users).get(user.id)
    if user_in_db and user_in_db.query:
        return user_in_db.query.type_query


def add_params_to_query(user, **kwargs):
    user_in_db = session.query(Users).get(user.id)
    if not user_in_db:
        user_in_db = add_user(user)
    query = user_in_db.query
    if not query:
        query = Query(user=user_in_db)
    for key, value in kwargs.items():
        setattr(query, key, value)
    session.add(query)
    session.commit()


def delete_query(user):
    user_in_db = session.query(Users).get(user.id)
    if user_in_db and user_in_db.query:
        user_in_db.last_use_datetime = datetime.utcnow()
        user_in_db.query_count += 1
        session.delete(user_in_db.query)
        session.commit()


def get_city_offset_from_utc(city_id):
    city_in_db = session.query(Cities).get(city_id)
    if city_in_db and city_in_db.dst_offset_from_utc is not None \
            and city_in_db.raw_offset_from_utc is not None \
            and city_in_db.offset_update_after_datetime is not None and \
                city_in_db.offset_update_after_datetime > datetime.utcnow():
        return city_in_db.dst_offset_from_utc, city_in_db.raw_offset_from_utc
    else:
        return None, None


def get_city_formatted_address(city_id):
    formatted_address = {}
    city_in_db = session.query(Cities).get(city_id)
    if city_in_db:
        if city_in_db.address_update_after_datetime is not None and \
                city_in_db.address_update_after_datetime > datetime.utcnow():
            try:
                formatted_address = \
                    json.loads(city_in_db.formatted_address)
            except:
                pass
    return formatted_address


def add_city_offset_from_utc(city_id, dst_offset_from_utc,
                             raw_offset_from_utc):
    city_in_db = session.query(Cities).get(city_id)
    if not city_in_db:
        city_in_db = Cities(id=city_id)
    city_in_db.dst_offset_from_utc = dst_offset_from_utc
    city_in_db.raw_offset_from_utc = raw_offset_from_utc
    city_in_db.offset_update_after_datetime = datetime.utcnow() + \
                                              OFFSET_UPDATE_INTERVAL
    session.add(city_in_db)
    session.commit()


def add_city_formatted_address(city_id, formatted_address,
                               lang=DEFAULT_LANGUAGE):
    city_in_db = session.query(Cities).get(city_id)
    if not city_in_db:
        city_in_db = Cities(id=city_id)
    formatted_address_dict = get_city_formatted_address(city_id)
    formatted_address_dict[lang] = formatted_address
    city_in_db.formatted_address = json.dumps(formatted_address_dict)
    city_in_db.address_update_after_datetime = datetime.utcnow() + \
                                               ADDRESS_UPDATE_INTERVAL
    session.add(city_in_db)
    session.commit()
